# 

## What is this?

This package is in place to help easily and quickly implement a robust, extremely fast csv to REST API.
You feed in a file location, define the delimiter, an axios object, and some other useful options - the lines are ingested and run through your defined callback function for each line.

- The callback you define for processing the line MUST return a valid axios object (headers and body, where applicible)
- 