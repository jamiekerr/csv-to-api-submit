var fs = require('fs'),
    es = require('event-stream'),
    axios = require('axios'),
    ex = exports

var streamFile = './testfile/indiv18/itcont.txt'
var streamOptions = {
    flags: 'r'
}
var axiosOptions = {}
var processStreamOptions = {
    concurrency: 10,
    failRetryLimit: 3,
    logFileName: null,
    fileDelimiter: '|'
}

var lineCount = 0
var retryProcessingQueue = []
var failedProcessingQueue = []

// Performance Measure
const {performance} = require('perf_hooks')
var t0, t1

var processStream = (streamFile, streamOptions) => {

    t0 = performance.now()

    let stream = fs.createReadStream(streamFile, streamOptions)

    stream
        .pipe(es.split())
        .pipe(es.mapSync((line) => {

            if (lineCount < 1000000) {
                if (process(line) == false) {
                    let retryItem = {
                        line: line,
                        retryCount: 1
                    }
                    retryProcessingQueue.push(retryItem)
                }
                // NOTE: For testing...
                // Add a 'check for error processing' function here (check the response from the API request).
            } else {
                stream.emit('end')
                stream.close()
            }

        }))
        .on('error', (err) => {
            console.log('Error processing file lines')
            console.log(err)
        })
        .on('close', () => {
            // On close, run the retry function for queued failed items, then run the processStreamEnd
            retryItems(retryProcessingQueue)
            processStreamEnd()
        })

}

var process = (line) => {
    lineCount ++
    let lineArray = line.split(processStreamOptions.fileDelimiter)
    // Do something with each line here, callback must return a axios object (head, body, options)
    let axiosObject = buildAxiosObject(line)

    // This is just for testing, on live will be when an error comes back for processing the axios request e.g. 400, 500, etc
    let random = Math.floor(Math.random() * 11)
    if(random == 1) {
        return false
        // throw new Error(`Error processing line: ${lineCount}`)
    }
    return true
    // This is just for testing, on live will be when an error comes back for processing the axios request e.g. 400, 500, etc
}

var processRetry = (retryLine, lineIndex) => {
    // console.log(`
    // retryLine.retryCount: ${retryLine.retryCount}
    // processStreamOptions.failRetryLimit: ${processStreamOptions.failRetryLimit}
    // `)
    if (retryLine.retryCount > processStreamOptions.failRetryLimit) {
        console.log(`
        Retry limit hit!
        failRetryLimit: ${processStreamOptions.failRetryLimit}
        Current retryCount: ${retryLine.retryCount}
        `)
        failedProcessingQueue.push(retryLine)
        retryProcessingQueue.splice( lineIndex, 1 )
    } else {
        let line = retryLine.line
        if (process(line) == true) {
            retryProcessingQueue.splice( lineIndex, 1 )
        } else {
            retryLine.retryCount += 1
            console.log('\n--------')
            console.log(`Failed again, ${retryLine.retryCount} of ${processStreamOptions.failRetryLimit} total attempts.`)
            console.log(retryLine.retryCount)
            console.log('--------\n')
        }
         
    }
}

var buildAxiosObject = (line) => {
    let axiosObject = {}
    let axiosData = {
        state: line[8]
    }
    axiosObject = {
        method: 'post',
        url: '/url',
        headers: {
            'X-Requested-With': 'XMLHttpRequest'
        },
        data: axiosData
    }
    return axiosObject
}

var retryItems = (retryProcessingQueue) => {
    // Run the retry process here
    console.log('There is retry items:', retryProcessingQueue.length)
    retryProcessingQueue.forEach( (retryLine, index) => {
        processRetry(retryLine, index)
    })
    if ( retryProcessingQueue.length ) {
        console.log('There is more items, we are going to retry again.')
        retryItems(retryProcessingQueue)
    }
}

var processStreamEnd = (data) => {
    // Clean up, close etc
    console.log('-----------------------')
    console.log('End count', lineCount)
    console.log('End of script')
    console.log('retryProcessingQueue', retryProcessingQueue.length)
    console.log('failedProcessingQueue', failedProcessingQueue.length)

    t1 = performance.now()
    let totalTime = (t1 - t0)
    console.log(`Call to run full process took ${totalTime} milliseconds // ${totalTime / 1000} seconds.`)
    if (retryProcessingQueue.length) {
        console.log('Running again!')
        console.log('-----------------------')
        retryProcessingQueue = []
        processStream(streamFile, streamOptions, axiosOptions)
    } else {
        console.log('All done.')
        console.log('-----------------------')
    }
}

// This will be the exported function, taking the stream file and stream options as params.
processStream(streamFile, streamOptions, axiosOptions, processStreamOptions)

// 
// StreamFile
// String, csv file to process

//
// StreamOptions
// Object, createReadStream options

//
// axiosOptions
// Object, axios options object

//
// processStreamOptions
// Object, other process options
/*

{
    concurrency: concurrent API request limit (number),
    failRetryLimit: number of retries for failed API submissions (number),
    logFileName: if provided will create a log file with verbose details (string),
    fileDelimiter: default is comma, if provided will overwrite comma with custom delimiter (string)
    ...
}

*/